# Industria Aperta

Questo progetto serve a raccogliere ed a mettere in ordine i problemi, le segnalazioni e le domande rivolte all'associazione.

* Chiunque, anche chi non è socio dell'associazione, può rivolgere una domanda sull'associazione, la segnalazione di un problema, una proposta di attività per l'associazione.

* Le segnalazioni più urgenti ed importanti e quelle che prevedono l'utilizzo di dati personali o dati riservati di attinenza dell'associazione, verranno spostate nel pannello delle segnalazioni accessibile solo al direttivo.

* Per fare delle segnalazioni che contengono dati personali o dati riservati dell'associazione, utilizzate sempre il [modulo di contatto](https://www.industriasoftwarelibero.it/contattaci/) che garantisce la riservatezza dei dati inviati.

* Tutti gli utenti sono pregati di rispettare la [netiquette](https://it.wikipedia.org/wiki/Netiquette#Regole_e_principi_della_netiquette) e la [policy dell'associazione](https://www.industriasoftwarelibero.it/privacy-policy/).

* Industria Italiana del Software Libero è una associazione a supporto della comunità professionale del software libero, ma allo stesso tempo ha bisogno del contributo della comunità per crescere e migliorarsi. Sii sempre costruttivo!

* Happy hacking!
